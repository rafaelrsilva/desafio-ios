//
//  main.m
//  DribbbleApp
//
//  Created by MacVM on 28/03/18.
//  Copyright © 2018 Rafael Ribeiro da Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
